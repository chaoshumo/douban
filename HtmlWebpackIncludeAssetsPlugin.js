const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

const assets = [
    'https://shuzz.site/static/react-router-dom/4.3.1/react-router-dom.min.js',
    'https://shuzz.site/static/modern-normalize/0.5.0/modern-normalize.min.css',
    'https://shuzz.site/static/lazysizes/4.1.5/lazysizes-umd.min.js',
    'https://shuzz.site/static/mobx/5.9.0/mobx.umd.min.js',
    'https://shuzz.site/static/mobx-react/5.4.3/index.min.js',
    'https://shuzz.site/static/styled-components/3.5.0-0/styled-components.min.js'
];
const dev = [
    'https://shuzz.site/static/react/16.7.0/react.development.js',
    'https://shuzz.site/static/react-dom/16.7.0/react-dom.development.js',
];
const prod = [
    'https://shuzz.site/static/react/16.7.0/react.production.min.js',
    'https://shuzz.site/static/react-dom/16.7.0/react-dom.production.min.js',
]
module.exports = function (env) {
    return new HtmlWebpackIncludeAssetsPlugin({
        assets: env === 'production' ? prod.concat(assets) : dev.concat(assets),
        append: false,
        publicPath: false
    });
}