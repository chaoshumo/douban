import { observable, action, runInAction } from "mobx";
import { getJson } from './api'

export class AppStore {

}
export class HomeStore {
    @observable movies = [];
    @observable total = 0;

    @action
    getMovies = async () => {
        try {
            const d = await getJson('count=10');
            runInAction(() => {
                this.movies = (<any>d).subjects;
                this.total = (<any>d).total;
            })

        } catch (error) {
            console.error(error)

        }
    }
}

export class MovieStore {
    @observable subjects = [];

    @action
    getSubjects = async () => {
        try {
            const d = await getJson('p=1&type=movie&tag=%E7%83%AD%E9%97%A8&sort=recommend&page_limit=20&page_start=0');
            runInAction(() => {
                this.subjects = (<any>d).subjects;
            })

        } catch (error) {
            console.error(error)

        }
    }
}

export class TvStore {
    @observable subjects = [];

    @action
    getSubjects = async () => {
        try {
            const d = await getJson('p=1&type=tv&tag=%E7%83%AD%E9%97%A8&sort=recommend&page_limit=20&page_start=0');
            runInAction(() => {
                this.subjects = (<any>d).subjects;
            })

        } catch (error) {
            console.error(error)

        }
    }
}

export default {
    appStore: new AppStore(),
    homeStore: new HomeStore(),
    movieStore: new MovieStore(),
    tvStore: new TvStore(),
}