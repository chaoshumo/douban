import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { HomeStore, MovieStore, TvStore } from '../../store';
import Thumbnail from 'src/components/Thumbnail'
import ScrollContainer from 'src/components/ScrollContainer'
import styled from 'styled-components';

interface PageProps {
    homeStore: HomeStore,
    tvStore: TvStore,
    movieStore: MovieStore
}

const H3=styled.div`
    font-size: 18px;
    font-weight: bold;
`

@inject("homeStore", "movieStore", "tvStore")
@observer
export default class Page extends React.Component<PageProps>{
    componentDidMount() {
        this.props.homeStore.getMovies();
        this.props.movieStore.getSubjects();
        this.props.tvStore.getSubjects();
    }
    render() {
        return (
            <div style={{padding: '20px 10px 10px 10px'}}>
                <H3>影院热映</H3>
                <ScrollContainer>
                    {this.props.homeStore.movies.map((m, i) => <Thumbnail title={m.title} cover={m.images.medium} id={m.id} rate={m.rating.average} key={i} />)}
                </ScrollContainer>
                <H3>热门电影</H3>
                <ScrollContainer>
                    {this.props.movieStore.subjects.map((m, i) => <Thumbnail {...m} key={i} />)}
                </ScrollContainer>
                <H3>热门电视剧</H3>
                <ScrollContainer>
                    {this.props.tvStore.subjects.map((m, i) => <Thumbnail {...m} key={i} />)}
                </ScrollContainer>
            </div>
        )
    }
}