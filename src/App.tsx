import * as React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import routers from "./router";
import './App.css'

class App extends React.Component {
  public render() {
    return (
      <Router>
        <Switch>
          {routers.map((route, i) => (
            <Route
              key={i}
              exact={true}
              path={route.path}
              component={route.component}
            />
          ))}

        </Switch>
      </Router>
    )
  }
}

export default App;