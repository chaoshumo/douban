import * as React from 'react'
import home from './pages/home'
import tv from './pages/tv'


export default [{
    path: '/',
    component: home
}, {
    path: '/tv',
    component: tv
}]