import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from "./App";

import { Provider } from 'mobx-react';
import { configure } from 'mobx';

import store from './store'

// configure({
//     enforceActions: 'observed'
// })


ReactDOM.render(
    <Provider {...store}><App /></Provider>,
    document.getElementById('app')
);