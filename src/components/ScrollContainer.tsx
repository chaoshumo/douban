import * as React from 'react';

import styled from 'styled-components'
const Wrapper = styled.div`
    overflow: hidden;
    overflow-x: auto;
    display: flex;
    -webkit-overflow-scrolling: touch;
    padding: 17px 0;
    min-height: 210px;
`;

export default class extends React.Component {
    render() {
        return (
            <Wrapper>{this.props.children}</Wrapper>
        )
    }
}


