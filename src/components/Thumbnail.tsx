import * as React from 'react';
import styled from 'styled-components';
import Rate from "src/components/Rate";


interface Props {
    title: string,
    cover: string,
    id: string,
    is_new?: boolean,
    rate: number
}

const Wrapper = styled.div`
    width: 100px;
    flex: none;
    :not(:first-child) {
        margin-left: 16px;
    }
`;

const Poster = styled.div`
    width: 100%;
    height: 142px;
    overflow: hidden;
    margin-bottom: 5px;

    img {
        max-width: 100%;
        height: 100%;
    }
`;

const Title = styled.div`
    white-space: nowrap;
    font-size: 12px;
    overflow: hidden;
    text-overflow: ellipsis;
    color: #111;
    text-align: center;
`;

export default class Thumbnail extends React.Component<Props>{
    render() {
        return (
            <Wrapper>
                <Poster>
                    <img data-src={this.props.cover} className="lazyload" />
                </Poster>
                <Title>{this.props.title}</Title>
                <Rate rate={(+this.props.rate).toFixed(1)} />
            </Wrapper>
        )
    }
}