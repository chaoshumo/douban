import * as React from 'react';
import styled from 'styled-components';
import * as yellowStar from 'src/images/star-yellow.png';
import * as grayStar from 'src/images/star-gray.png';


interface RateProps {
    rate: number | string
}

const Star = styled.img`
    display: inline-block;
    width: 10px;
    height: 10px;
    margin-right: 1px;
`;

const Rate = styled.span`
    color: #aaa;
    font-size: 12px;
`;

export default class extends React.Component<RateProps> {
    render() {
        const rate = (+this.props.rate) / 2;
        const integer = Math.round(rate);
        const integerArr = new Array(integer).fill('*');
        const remainderArr = integerArr.slice(0, (50 - integer * 10) / 10);

        return (
            <div style={{textAlign:'center'}}>
                {integerArr.map((e, i) => <Star src={yellowStar} key={i} />)}
                {remainderArr.map((e, i) => <Star src={grayStar} key={i} />)}
                <Rate>{this.props.rate}</Rate>
            </div>
        )
    }
}