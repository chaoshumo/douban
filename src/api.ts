const base = 'https://api.douban.com/v2';

export const getJsonp = (url: string, cb: (d: any) => void) => {
    const script = document.createElement('script');
    script.type = 'text/javascript';


    script.src = base + url + '&callback=callbackFn';
    document.head.appendChild(script);
    (<any>window).callbackFn = cb;
}

export const getJson = (querystring: string) => {
    return new Promise((resolve, reject) => {
        fetch(`/api/?${querystring}`)
            .then(function (response) {
                return response.json();
            })
            .then(resolve);
    })
}