const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const HtmlWebpackIncludeAssetsPlugin = require('./HtmlWebpackIncludeAssetsPlugin');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'source-map',
    plugins: [
        HtmlWebpackIncludeAssetsPlugin()
    ],
    devServer: {
        port: 3000,
        host: '0.0.0.0',
        contentBase: path.join(__dirname, 'dist'),
        compress: false,
        filename: 'index.html',
        open: false,
        publicPath: '/',
        historyApiFallback: true,// false刷新404
        proxy: {
            '/api': 'http://localhost:3005'
        }
    }
});