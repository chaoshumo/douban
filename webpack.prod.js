const merge = require('webpack-merge');
const webpack = require('webpack');
const common = require('./webpack.common.js');
const CompressionPlugin = require("compression-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('./HtmlWebpackIncludeAssetsPlugin');

const path = require('path');

module.exports = merge(common, {
    mode: 'production',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'js/[id].[contenthash].js'
    },
    resolve: {
        alias: {

        }
    },
    plugins: [
        HtmlWebpackIncludeAssetsPlugin('production'),
        new CleanWebpackPlugin(['dist']),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new CompressionPlugin({
            test: new RegExp('\\.(js|css)$'),
            algorithm: 'gzip',
        })
    ]
});