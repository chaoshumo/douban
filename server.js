const Koa = require('koa');
const app = new Koa();
const request = require('superagent');
const bases = [
    'https://api.douban.com/v2/movie/in_theaters?',// 影院热映
    'https://movie.douban.com/j/search_subjects?'// 电视剧
]

app.use(async ctx => {
    const { p } = ctx.request.query;
    const { body } = await request
        .get(bases[p || 0] + ctx.request.querystring);

    ctx.body = { url: bases[p || 0] + ctx.request.querystring, ...body };
});

app.listen(3005);